package miet;

import java.util.ArrayList;

public class Report {
	public int dayToEnd;
	public int totalPlace;
	public int vacancytPlace; 
	
	public int totalPlaceCampus;
	public int vacancyCampus; 

	public int place;
	public int placeCampus;

	public ArrayList<RowReport> report;
	public ArrayList<RowReport> reportCampus;
	
	public Report()
	{
		this.report = new ArrayList<RowReport>();		
		this.reportCampus = new ArrayList<RowReport>();		
	}
}